from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Visualizing the efficiency of my Kia Soul EV',
    author='Hugh Brown',
    license='MIT',
)
