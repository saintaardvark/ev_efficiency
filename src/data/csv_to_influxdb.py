#!/usr/bin/env python3

import datetime
import json
import logging
import os
import time

import click
from influxdb import InfluxDBClient
import numpy as np
import pandas as pd
import seaborn

@click.group()
def ev_stats():
    """
    A tool to import EV stats into InfluxDB.
    """


def import_csv(csv_file, dtypes):
    """
    Import csv_file with specified dtypes and return pandas dataframe.

    Index processing is hard-coded.
    """
    logger = logging.getLogger(__name__)
    logger.info(f"Reading {csv_file.name}...")

    df = pd.read_csv(
        csv_file,
        sep=",",
        dtype=dtypes,
        parse_dates=["Date"],
        index_col=0,
    )
    df.index = pd.to_datetime(df.index)

    logger.debug(df)
    logger.debug(df.dtypes)
    logger.debug(df.index.dtype)

    return df


def build_efficiency_influxdb_data(df):
    """
    Build influxdb data out of dataframe and return
    """
    logger = logging.getLogger(__name__)
    logger.info("Building influxdb data...")

    influx_data = []
    for index, row in df.iterrows():
        try:
            ts = index.timestamp()  # seconds since epoch, as float
        except Exception as exc:
            logger.warn(f"Skipping row [{row}] because of exception: {exc}")
            continue
        logger.debug(ts)
        km_driven = row[0]
        kwh_per_100km = row[1]
        if np.isnan(km_driven) or np.isnan(kwh_per_100km):
            logger.warn(
                f"Skipping row [{row}] because km_driven({km_driven}) or kwh_per_100km({kwh_per_100km}) is nan"
            )
            continue
        measurement = {
            "measurement": "efficiency",
            "fields": {
                "km_driven": km_driven,
                "kwh_per_100km": kwh_per_100km,
            },
            "time": index,
        }
        influx_data.append(measurement)

    logger.debug(influx_data)
    return influx_data


def build_odometer_battery_distance_data(df):
    """
    Build influxdb data out of dataframe and return
    """
    logger = logging.getLogger(__name__)
    logger.info("Building influxdb data...")

    influx_data = []
    last_odometer = 0
    # The very first range estimate was 407 km; we'll use that for the
    # zeroth value.
    last_range_estimate = 407
    for index, row in df.iterrows():
        try:
            ts = index.timestamp()  # seconds since epoch, as float
        except Exception as exc:
            logger.warn(f"Skipping row [{row}] because of exception: {exc}")
            continue
        logger.debug(ts)
        odometer = row["Odometer (km)"]
        if np.isnan(odometer):
            odometer = last_odometer
        last_odometer = odometer  # now set for next time

        battery_percent = row["Battery percentage"]
        if np.isnan(battery_percent):
            # Not worth it, skip
            continue

        range_estimate = row["Estimated Distance Remaining (km)"]
        if np.isnan(range_estimate):
            # Not worth it, skip
            continue

        if range_estimate > last_range_estimate:
            # We've charged; set the diff to 0 to account for this
            diff_in_range_estimate = 0
        else:
            diff_in_range_estimate = last_range_estimate - range_estimate

        measurement = {
            # TODO: range_estimate is a good name, but doesn't match
            # the "odometer_battery_distance" convention I've adopted
            # so far; that convention should probably change.
            "measurement": "range_estimate",
            "fields": {
                "km_driven": row["km_driven"],
                "odometer": odometer,
                "battery_percent": battery_percent,
                "range_estimate": range_estimate,
                "range_estimate_diff": row["range_estimate_diff"],
                "difference_between_distance_driven_and_estimated_range_consumed": row[
                    "difference_between_distance_driven_and_estimated_range_consumed"
                ],
            },
            "time": index,
        }
        influx_data.append(measurement)

    logger.debug(influx_data)
    return influx_data


def build_influxdb_client():
    """
    Build and return InfluxDB client
    """
    # Setup influx client
    logger = logging.getLogger(__name__)

    DB = "ev_efficiency"
    host = "home.saintaardvarkthecarpeted.com"
    port = "26472"
    INFLUX_USER = "ev_efficiency"
    INFLUX_PASS = os.getenv("INFLUX_PASS")

    influx_client = InfluxDBClient(
        host=host,
        port=port,
        username=INFLUX_USER,
        password=INFLUX_PASS,
        database=DB,
        ssl=True,
        verify_ssl=True,
    )
    logger.info("Connected to InfluxDB version {}".format(influx_client.ping()))
    return influx_client


def write_influx_data(influx_data, influx_client):
    """
    Write influx_data to database
    """
    logger = logging.getLogger(__name__)
    logger.info("Writing data to influxdb...")

    influx_client.write_points(influx_data, time_precision="s")


@click.command(
    "dashboard_ev_efficiency", short_help="Import dashboard EV efficiency CSV file"
)
@click.argument("csv_file", type=click.File("r"))
def dashboard_ev_efficiency(csv_file):
    """
    Import dashboard EV efficiency CSV file
    """
    logger = logging.getLogger(__name__)

    # TODO: Those dates should be at end of day, rather than beginning of day
    df = import_csv(
        csv_file, dtypes={"Date": "str", "Km": "float", "kwh/100km": "float"}
    )
    influx_data = build_efficiency_influxdb_data(df)
    influx_client = build_influxdb_client()
    write_influx_data(influx_data, influx_client)
    logger.info("Done!")


@click.command(
    "odometer_battery_distance", short_help="Import odometer battery distance CSV file"
)
@click.argument("csv_file", type=click.File("r"))
@click.argument("chart", type=click.File("r"))
def odometer_battery_distance(csv_file):
    """
    Import odometer battery distance CSV file
    """
    logger = logging.getLogger(__name__)
    # "Date","Odometer (km)","Battery percentage","Estimated Distance Remaining (km)","Charge?","Charge time","Charge cost","Notes","Weather",,,,
    df = import_csv(
        csv_file,
        dtypes={
            "Date": "str",
            "Odometer (km)": "float",
            "Battery percentage": "float",
            "Estimated Distance Remaining (km)": "float",
            "Charge?": "str",
            "Charge time": "str",
            "Charge cost": "str",
            "Notes": "str",
            "Weather": "str",
        },
    )
    df.drop(df.iloc[:, 3:], inplace=True, axis=1)
    # diff_dataframe = df.diff()
    df["km_driven"] = df["Odometer (km)"] - df["Odometer (km)"].shift(1)
    df["range_estimate_diff"] = (
        df["Estimated Distance Remaining (km)"].shift(1)
        - df["Estimated Distance Remaining (km)"]
    )
    df["difference_between_distance_driven_and_estimated_range_consumed"] = (
        df["km_driven"] - df["range_estimate_diff"]
    )

    logger.info(df)
    influx_data = build_odometer_battery_distance_data(df)
    # import pprint
    # pp = pprint.PrettyPrinter(indent=4)
    # pp.pprint(influx_data)

    influx_client = build_influxdb_client()
    write_influx_data(influx_data, influx_client)


ev_stats.add_command(dashboard_ev_efficiency)
ev_stats.add_command(odometer_battery_distance)

if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    ev_stats()
