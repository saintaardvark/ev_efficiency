#!/usr/bin/env python3

import click
import logging
from pathlib import Path
import subprocess


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')
    subprocess.run(["libreoffice",
                    "--headless",
                    "--convert-to",
                    "csv",
                    "--outdir",
                    output_filepath,
                    input_filepath])


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various
    # files; keeping for future reference.
    # project_dir = Path(__file__).resolve().parents[2]

    main()
