# ev_efficiency

Visualizing the efficiency of my Kia Soul EV

# Possible future options

- https://docs.openvehicles.com/en/latest/components/vehicle_kiasoulev/docs/index.html

# Usage

There are two sets of stats I'm collecting, each with different
frequency.  These sets are both recorded in the same spreadsheet:
`data/raw/car_ev_stats.ods`.

The first tab of that spreadsheet, `DashboardEVEfficiencyStats`,
holds the car's own records of efficiency:

- date
- `km`: kilometres driven
- `kwH/100km`: kiloWatt-hours per 100 km

This data is rotated regularly from the dashboard, and I don't collect
it regularly.  Currently, the `Makefile` target imports this to
InfluxDB -- but doesn't do any sanity checking on the data, meaning
you might import the next dataset by mistake and get garbage data.

The second tab of that spreadsheet, `OdometerBatteryDistance`, holds
my own records of the car's performance:

- date
- odometer at end of drive (km)
- battery percentage at end of drive
- the car's estimate of remaining range (km)
- and occasionally notes about charging, cost, weather, etc.

I *do* record this data with just about every drive.  What I want to
get out of this is an idea of how much to trust the car's remaining
range estimate.

Currently, I export these tabs by hand as CSV files, meaning I need to
be careful to name the exported file appropriately.

# Data entry
Data entry can be done by opening the spreadsheet
`data/raw/car_ev_stats.ods`.

Currently, I export these tabs by hand as CSV files, meaning I need to
be careful to name the exported file appropriately.

Importing data to InfluxDB can be done by running:

```
# Default target is `import`
make
```

See note above about how this currently expects this to be the
`DashboardEVEfficiencyStats` data, but does *not* sanity-check it!

# Thanks!

Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience
